package com.example.finalterm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_web_view.*

class ActivityWebView : AppCompatActivity() {
    private var url = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        url = intent.getStringExtra("url")
        Log.d("data", url)
        init()
    }

    private fun init() {
        back_button_web_view.setOnClickListener {
            startActivity(Intent(this, ActivityHome::class.java))
        }
        web_view_title.text = url
        webView.settings.loadsImagesAutomatically = true
        webView.settings.javaScriptEnabled = true
        webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        webView.webViewClient = WebViewClient()
        webView.loadUrl(url)
    }
}
