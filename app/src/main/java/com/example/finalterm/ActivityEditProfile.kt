package com.example.finalterm

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.userProfileChangeRequest
import kotlinx.android.synthetic.main.edit_profile_layout.*

class ActivityEditProfile : AppCompatActivity() {
    private var auth = FirebaseAuth.getInstance()
    var user = auth.currentUser
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_profile_layout)
        init()
    }

    private fun init() {
        editProfileBackButton.setOnClickListener {
            startActivity(Intent(this, ActivityProfile :: class.java))
        }
        save_changes.setOnClickListener {
            updateProfile(editName.text.toString(), editUrl.text.toString())
            Toast.makeText(baseContext, "Saved", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, ActivityProfile :: class.java))
        }
    }

    private fun updateProfile(username : String, photoUrl : String) {
        val profileUpdates = userProfileChangeRequest {
            displayName = username
            photoUri = Uri.parse("$photoUrl")
        }

        user!!.updateProfile(profileUpdates)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d("my", "User profile updated.")
                }
            }
    }

}