package com.example.finalterm


import android.util.Log.d
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*

// // top-headlines?        country=us      apiKey=a764e4d1917d49e8ba3c5139734bf057         getRequest() STANDARD
// // everything?           q=bitcoin       apiKey=a764e4d1917d49e8ba3c5139734bf057         getRequestFromQuery()


object DataLoader {
    private var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("http://newsapi.org/v2/")
        .build()

    private var service = retrofit.create(ApiRetrofit::class.java)

    fun getRequest(path: String, country: String, pageSize: String, apiKey : String, customCallback: CustomCallback) {
        val call = service.getRequest(path, country, pageSize, apiKey)
        call.enqueue(callback(customCallback))
    }

    fun getRequestDate(path: String, q: String, from: String, to: String, sortBy: String, pageSize: String, apiKey : String, customCallback: CustomCallback) {
        val call = service.getRequestDate(path, q, from, to, sortBy, pageSize, apiKey)
        call.enqueue(callback(customCallback))
    }

    fun getRequestForQuery(path: String, q: String, pageSize: String, apiKey : String, customCallback: CustomCallback) {
        val call = service.getRequestForQuery(path, q, pageSize, apiKey)
        call.enqueue(callback(customCallback))
    }

    fun postRequest(path:String, parameters: MutableMap<String, String>,customCallback: CustomCallback) {
        val call = service.postRequest(path, parameters)
        call.enqueue(callback(customCallback))
    }

    private fun callback(customCallback: CustomCallback) = object: Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            d("getRequest", "${t.message}")
            customCallback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            d("getRequest", "${response.body()}")
            customCallback.onSuccess(response.body().toString())
        }

    }
}

interface ApiRetrofit {
    @GET("{path}")
    fun getRequest(
        @Path("path") path: String?,
        @Query("country") country : String,
        @Query("pageSize") pageSize: String,
        @Query("apiKey") apiKey: String): Call<String>

    @GET("{path}")
    fun getRequestForQuery(
        @Path("path") path: String?,
        @Query("q") q : String,
        @Query("pageSize") pageSize: String,
        @Query("apiKey") apiKey: String): Call<String>

    @GET("{path}")
    fun getRequestDate(
        @Path("path") path: String?,
        @Query("q") q: String,
        @Query("from") from:String,
        @Query("to") to: String,
        @Query("sortBy") sortBy: String,
        @Query("pageSize") pageSize: String,
        @Query("apiKey") apiKey: String): Call<String>

    @FormUrlEncoded
    @POST("{path}")
    fun postRequest(@Path("path") path: String?, parameters:Map<String, String>): Call<String>
}