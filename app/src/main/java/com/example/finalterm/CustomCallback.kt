package com.example.finalterm

interface CustomCallback {
    fun onSuccess(result: String){}
    fun onFailure(errorMassage: String){}
}