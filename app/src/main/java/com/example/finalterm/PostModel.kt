package com.example.finalterm

import com.google.gson.annotations.SerializedName

class PostModel {
    var status = ""
    var totalResults = 0
    lateinit var articles:MutableList<Data>

    class Data {
        var source: Source = Source()
        var author = ""
        var title = ""
        var description = ""
        var url = ""
        var urlToImage = ""
        var publishedAt = ""
        var content = ""
    }

    class Source {
        var id: String? = null
        var name: String = ""
    }
}