package com.example.finalterm

import android.graphics.drawable.Drawable
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.*
import java.text.SimpleDateFormat

class RecyclerViewAdapterPosts(private val posts:MutableList<PostModel.Data>, private val activity: ActivityHome, private val rclView: RecyclerView):
    RecyclerView.Adapter<RecyclerViewAdapterPosts.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_posts_recyclerview_layout, parent, false))
    }

    override fun getItemCount() = posts.size

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var model:PostModel.Data
        @RequiresApi(Build.VERSION_CODES.O)
        fun onBind() {
            model = posts[adapterPosition]
            Glide.with(activity).load(model.urlToImage).listener(object : RequestListener<Drawable>{
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {

                    itemView.progress_load_photo.visibility = View.GONE
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    itemView.progress_load_photo.visibility = View.GONE
                    return false
                }

            }).transition(DrawableTransitionOptions.withCrossFade()).into(itemView.img)
            itemView.title.text = model.title
            itemView.desc.text = model.description
            itemView.author.text = model.author
            val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            val outPutFormat = SimpleDateFormat("EEE MMM yyyy")
            val date = inputFormat.parse(model.publishedAt)
            itemView.publishedAt.text = outPutFormat.format(date)
            itemView.source.text = model.source.name


            itemView.setOnClickListener() {
                rclView.layoutManager = LinearLayoutManager(activity)
                var pAdapt = PostView(model , activity)
                rclView.adapter = pAdapt
            }
        }
    }
}