package com.example.finalterm

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.read_later_post_view_layout.view.*
import java.text.SimpleDateFormat


class PostViewReadLater(private val post:PostModel.Data, private val activity: ActivityReadLater): RecyclerView.Adapter<PostViewReadLater.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.read_later_post_view_layout, parent, false))
    }

    override fun getItemCount() = 1

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind() {

            Glide.with(activity).load(post.urlToImage).into(itemView.img)
            itemView.title.text = post.title
            itemView.desc.text = post.description
            itemView.author.text = post.author
            val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            val outPutFormat = SimpleDateFormat("EEE-MMM-yyyy")
            val date = inputFormat.parse(post.publishedAt)
            itemView.publishedAt.text = outPutFormat.format(date)
            itemView.source.text = post.source.name

            itemView.discard_post.setOnClickListener {
                activity.modifyData(post)
                Toast.makeText(activity, "Post Removed From List", Toast.LENGTH_SHORT).show()
                activity.init()
            }

            itemView.open_article_page.setOnClickListener {
                activity.openWebView(post.url)
            }
        }
    }
}