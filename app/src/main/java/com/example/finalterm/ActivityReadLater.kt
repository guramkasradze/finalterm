package com.example.finalterm

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_read_later.*

class ActivityReadLater : AppCompatActivity() {
    lateinit var posts : ArrayList<PostModel.Data>
    private var auth = FirebaseAuth.getInstance()
    private val user = auth.currentUser
    private val mail = user?.email

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_read_later)

        var myJson = intent.extras?.getString("readLater", "string")
        var type = object : TypeToken<ArrayList<PostModel.Data>>(){}.type
        if (!myJson.isNullOrEmpty())
            posts = Gson().fromJson(myJson, type)
        else
            posts = ArrayList<PostModel.Data>()

        init()
    }

    fun init() {

        readLaterBackButton.setOnClickListener {
            startActivity(Intent(this, ActivityHome :: class.java))
        }
        readLaterClearButton.setOnClickListener {
            clearData()

            val intent = Intent(this, ActivityReadLater ::class.java)
            intent.putExtra("readLater", "")
            startActivity(intent)
        }

        recyclerViewReadLater.layoutManager = LinearLayoutManager(this)
        val adapter = RecyclerViewAdapterReadLater(posts, this, recyclerViewReadLater)
        recyclerViewReadLater.adapter = adapter
    }

    private fun clearData() {
        posts.clear()
        var sharedPreferences = getSharedPreferences(mail.toString(), Context.MODE_PRIVATE)
        var editor = sharedPreferences.edit()
        var myJson = Gson().toJson(posts)
        editor.putString("read later", myJson)
        editor.apply()
    }

    fun modifyData(post: PostModel.Data) {
        posts.removeAll { it.title == post.title && it.description == post.description && it.publishedAt == post.publishedAt }
        var sharedPreferences = getSharedPreferences(mail.toString(), Context.MODE_PRIVATE)
        var editor = sharedPreferences.edit()
        var myJson = Gson().toJson(posts)
        editor.putString("read later", myJson)
        editor.apply()
    }

    fun openWebView(url: String) {
        var intent = Intent(this, ActivityWebView::class.java)
        intent.putExtra("url", url)
        startActivity(intent)
    }
}
