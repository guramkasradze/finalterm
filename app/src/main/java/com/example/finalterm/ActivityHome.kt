package com.example.finalterm

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_home.*

class ActivityHome : AppCompatActivity() {
    private var menuSearch: Menu? = null
    private var activity = this
    var readLater = ArrayList<PostModel.Data>()
    private var myJson: String? = null
    private var auth = FirebaseAuth.getInstance()
    private val user = auth.currentUser
    private val mail = user?.email

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        loadData()
        loadFragment(FragmentHome(this, "a764e4d1917d49e8ba3c5139734bf057"), "HomeFragment")

        bottomNavigationView.setOnNavigationItemSelectedListener (navListener)
    }

    private var navListener : BottomNavigationView.OnNavigationItemSelectedListener =  BottomNavigationView.OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.dock_home -> { Log.d("myTag", "HOME")
                loadFragment(FragmentHome(activity, "a764e4d1917d49e8ba3c5139734bf057"), "HomeFragment")
                menuSearch?.findItem(R.id.menu_search)?.isVisible = true
                return@OnNavigationItemSelectedListener true
            }
            R.id.dock_search -> {Log.d("myTag", "search")
                loadFragment(FragmentSearchDate(activity), "FragmentSearchDate")
                menuSearch?.findItem(R.id.menu_search)?.isVisible = true
                return@OnNavigationItemSelectedListener true
            }
            R.id.dock_profile -> {Log.d("myTag", "profile")
                loadFragment(FragmentProfile(activity, myJson), "ProfileFragment")
                menuSearch?.findItem(R.id.menu_search)?.isVisible = false
                return@OnNavigationItemSelectedListener true
            }
            else -> {
                Log.d("myTag", "BAD")
                return@OnNavigationItemSelectedListener false
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.top_menu, menu)
        menuSearch = menu
        val searchItem = menu?.findItem(R.id.menu_search)

        if (searchItem != null) {
            val searchView = searchItem.actionView as SearchView

            searchView.queryHint = "Search Latest News"

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    if (!query.isNullOrEmpty()){
                        loadFragment(FragmentHome(activity, query), "HomeFragment")
                        val searchItem = menu.findItem(R.id.menu_search)
                        val mSearchView =
                            menu.findItem(R.id.menu_search).actionView as SearchView
                        if (mSearchView != null && searchItem != null) {
                            mSearchView.clearFocus()
                            searchItem.collapseActionView()
                        }
                    }

                    searchView.setQuery("", false)
                    searchView.clearFocus()
                    searchView.onActionViewCollapsed()

                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    //if (!newText.isNullOrEmpty())
                        //loadFragment(HomeFragment(activity, newText), "HomeFragment")

                    return true
                }
            })
        }

        return super.onCreateOptionsMenu(menu)
    }

    private fun loadFragment(fragment: Fragment, tag: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.home_screen, fragment, tag)
        transaction.commit()
    }

    fun signOut() {
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(this, ActivityLogIn :: class.java))
    }

    fun saveData(post: PostModel.Data) {
        readLater.add(post)
        var sharedPreferences = getSharedPreferences(mail.toString(), Context.MODE_PRIVATE)
        var editor = sharedPreferences.edit()
        var myJson = Gson().toJson(readLater)
        editor.putString("read later", myJson)
        editor.apply()
        readLater.clear()
        loadData()
    }

    fun loadData() {
        var sharedPreferences = getSharedPreferences(mail.toString(), Context.MODE_PRIVATE)
        myJson = sharedPreferences.getString("read later", null)
        var type = object : TypeToken<ArrayList<PostModel.Data>>(){}.type
        if (myJson != null)
            readLater = Gson().fromJson(myJson, type)
    }

    fun openWebView(url: String) {
        var intent = Intent(activity, ActivityWebView::class.java)
        intent.putExtra("url", url)
        startActivity(intent)
    }

    fun profileFrag() {
        loadFragment(FragmentProfile(activity, myJson), "ProfileFragment")
        menuSearch?.findItem(R.id.menu_search)?.isVisible = false
    }
}