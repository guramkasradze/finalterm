package com.example.finalterm

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_profile.*

class FragmentProfile(cntx : ActivityHome, myJson: String?): Fragment() {
    private var activity = cntx
    private var auth = FirebaseAuth.getInstance()
    private val user = auth.currentUser
    private var name :String? = ""
    private var email :String? = ""
    private var uid :String? = ""
    private var photoUrl : Uri? = Uri.EMPTY
    private var list  = myJson

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        init()
    }



    private fun init() {
        getCurrentUserInfo()

        if (photoUrl != null) { Glide.with(this).load(photoUrl).into(profileImageView) }
        else { Glide.with(this).load("https://i.gifer.com/S9OW.gif").into(profileImageView)}


        open_profile.setOnClickListener {
            open_profile.setBackgroundColor(Color.parseColor("#aba9a9"))
            startActivity(Intent(activity, ActivityProfile::class.java))
        }

        read_later.setOnClickListener {
            read_later.setBackgroundColor(Color.parseColor("#aba9a9"))
            val intent = Intent(activity, ActivityReadLater ::class.java)
            intent.putExtra("readLater", list)
            startActivity(intent)
        }

        sign_out_button.setOnClickListener {
            sign_out_button.setBackgroundColor(Color.parseColor("#aba9a9"))
            activity.signOut()
        }
    }

    private fun getCurrentUserInfo() {
        if (user != null) {
            name = user.displayName
            email = user.email
            uid = user.uid
            photoUrl =user.photoUrl
        }

        Log.d("my", "$name $email $uid $photoUrl")
    }
}