package com.example.finalterm

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_home.*

class FragmentHome(myContext : ActivityHome, str : String): Fragment() {
    var activity = myContext
    var chosen = str
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (chosen.equals("a764e4d1917d49e8ba3c5139734bf057"))
            getHomePosts()
        else
            getSearchPosts(chosen)

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    private fun getHomePosts() {

        DataLoader.getRequest("top-headlines", "us", "100", "a764e4d1917d49e8ba3c5139734bf057", object: CustomCallback{
            override fun onSuccess(result: String) {
                val model = Gson().fromJson(result, PostModel::class.java)
                Log.d("userCount", "${model.articles.size}")

                init(model)
            }
        })

    }

    private fun getSearchPosts(value: String?) {
        if (!value.isNullOrEmpty()) {
            DataLoader.getRequestForQuery("everything", value, "100", "a764e4d1917d49e8ba3c5139734bf057", object: CustomCallback{
                override fun onSuccess(result: String) {
                    Log.d("result", result)
                    val model = Gson().fromJson(result, PostModel::class.java)
                    Log.d("userCount", "${model.articles.size}")

                    init(model)
                }
            })
        }
    }

    private fun init(model:PostModel) {
        Log.d("size", model.articles.size.toString())
        if (model.articles.size > 0) {
            recyclerView.layoutManager = LinearLayoutManager(activity)
            val adapter = RecyclerViewAdapterPosts(model.articles, activity, recyclerView)
            recyclerView.adapter = adapter
        }
        else {
            model.articles.add(PostModel.Data())
            recyclerView.layoutManager = LinearLayoutManager(activity)
            val adapter = NotFoundRecyclerView(model.articles)
            recyclerView.adapter = adapter
        }

    }

}