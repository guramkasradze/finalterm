package com.example.finalterm

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_search_date.*
import java.util.*

class FragmentSearchDate(myContext : ActivityHome): Fragment() {
    var activity = myContext
    var dF = 0
    var mF = 0
    var yF = 0
    var dT = 0
    var mT = 0
    var yT = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_search_date, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        init()
    }

    private fun init() {
        var c = Calendar.getInstance()
        var year = c.get(Calendar.YEAR)
        var month = c.get(Calendar.MONTH)
        var day = c.get(Calendar.DAY_OF_MONTH)


        date_from.setOnClickListener {
            var dpFrom = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                date_from_text.text = "${dayOfMonth}/${month + 1}/${year}"
                dF = dayOfMonth
                mF = month + 1
                yF = year
            }, year, month, day)
            dpFrom.show()
        }


        date_to.setOnClickListener {
            var dpTo = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                date_to_text.text = "${dayOfMonth}/${month + 1}/${year}"
                dT = dayOfMonth
                mT = month + 1
                yT = year
            }, year, month, day)
            dpTo.show()
        }

        search_date.setOnClickListener {
            if (checkValidity() && !search_key.text.toString().isNullOrEmpty()) {
                getPosts(search_key.text.toString())
            }
            else {
                Toast.makeText(activity, "Input Date and Key Word", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun checkValidity() : Boolean{
        return if (yT < yF) {
            false
        } else if (yT == yF) {
            if (mF == mT) {
                dF <= dT
            } else mF <= mT
        } else {
            true
        }
    }

    private fun getPosts(info: String) {
        DataLoader.getRequestDate("everything", info, "$yF-$mF-$dF", "$yT-$mT-$dT", "relevancy", "100", "a764e4d1917d49e8ba3c5139734bf057", object: CustomCallback{
            override fun onSuccess(result: String) {
                if (result != "null") {
                    val model = Gson().fromJson(result, PostModel::class.java)
                    Log.d("infogg", model.articles.toString())
                    setView(model)
                }
                else {
                    var md = PostModel()
                    setView(md)
                }
            }
        })
    }

    private fun setView(model: PostModel) {
        if (model.articles.size > 0) {
            searchRecyclerView.layoutManager = LinearLayoutManager(activity)
            val adapter = RecyclerViewAdapterDate(model.articles, activity, searchRecyclerView)
            searchRecyclerView.adapter = adapter
        }
        else{
            searchRecyclerView.layoutManager = LinearLayoutManager(activity)
            val adapter = NotFoundRecyclerView(model.articles)
            searchRecyclerView.adapter = adapter
        }
    }

}