package com.example.finalterm

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_profile.*

class ActivityProfile : AppCompatActivity() {
    private var auth = FirebaseAuth.getInstance()
    private val user = auth.currentUser
    private var name :String? = ""
    private var email :String? = ""
    private var uid :String? = ""
    private var photoUrl : Uri? = Uri.EMPTY


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        init()
    }


    private fun init() {
        getCurrentUserInfo()

        if (photoUrl != null) { Glide.with(this).load(photoUrl).into(profileImageView) }
        else {Glide.with(this).load("https://i.gifer.com/S9OW.gif").into(profileImageView)}

        profileIdTextView.text = "#id" + uid
        profileNameTextView.setText(name)
        profileEmailTextView.text = email

        profileBackButton.setOnClickListener {
            startActivity(Intent(this, ActivityHome :: class.java))
        }

        profileEditButton.setOnClickListener {
            startActivity(Intent(this, ActivityEditProfile :: class.java))
        }
    }

    private fun getCurrentUserInfo() {
        if (user != null) {
            name = user.displayName
            email = user.email
            uid = user.uid
            photoUrl =user.photoUrl
        }

        Log.d("my", "$name $email $uid $photoUrl")
    }


}
